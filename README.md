# Travail pratique: Laravel

## Objectif

Développer une API REST sécuritaire en utilisant Laravel.

## Conditions de réalisation du travail

Votre remise consiste à livrer toutes les routes répondants aux demandes énoncées plus bas.
Vous devez utiliser les données fournies en SQL comme données de départ de votre application.

Le modèle de la base de données est disponible en annexe.

### Éléments importants de l'API REST :
- Contrôleurs
- Modèles et entité(s)
    - Relations appropriées
- Ressource (DTO)
- Validation (Request) pour les actions store et update
- Structure et données de départ (migration et seed)
- Authentification avec Sanctum

## API – Routes à développer
- **Opérations CRUD** pour un film :
    - **Consultation** des films (sans critiques et sans acteurs)
    - **Ajout** d’un film (seulement si admin)
    - **Consultation** d’un certain film avec ces critiques
    - **Suppression** d’un film (seulement si admin)
- **Consultation** de tous les **acteurs** d’un certain **film**
- **Ajout** d’une critique (seulement si membre connecté)
    - Un membre peut seulement écrire une critique par film
- **Recherche** de films
    - Selon les critères suivants :
        - Mot-clé (dans title et description) → keywords
        - Classification → rating
        - Durée maximale → max-length
    - Une **limite de 20 films** doit être retourné par requête (les 20 premiers correspondant aux critères)
        - Il est possible de préciser la **page** pour accéder à 20 autres films
    - Tous les **critères** sont **optionnels** et si aucun n’est fourni, on doit simplement retourner les 20 premiers films
- **Opérations CRUD** pour un *user* :
    - **Consultation** des informations d’un certain *user* (seulement si on est connecté avec ce *user*)
    - **Ajout** d’un nouveau *user*
    - **Modification** d’un *user* existant (seulement si on est connecté avec ce *user*)
        - Traiter le mot de passe séparément
- **Opérations d’authentification** pour un *user* :
    - **Connexion** d’un *user*
    - **Déconnexion** d’un *user*

<br>
<hr>
<br>

![Diagramme](DiagrammeModeleBaseDeDonnees.jpg)