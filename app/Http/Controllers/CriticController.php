<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Critic;
use App\Http\Resources\CriticResource;
use App\Http\Requests\CriticRequest;
use Illuminate\Support\Facades\Auth;
use Exception;

class CriticController extends Controller
{
    public function store(CriticRequest $request)
    {
        if(Auth::check())
        {
            try
            {
                $donnees = $request->validated();
                $userId = auth()->user()->id;
                $nombreCritic = Critic::where('film_id', $request['film_id'])->where('user_id', $userId)->count();

                if($nombreCritic < 1)
                {
                    $critic = Critic::create($donnees);
                    return (new CriticResource($critic))->response()->setStatusCode(201);
                }
                else
                {
                    abort(403);
                }
            }
            catch(Exception $e)
            {
                abort(422,'Validation problem');
            }
        }
        else
        {
            abort(401);
        }
    }
}
