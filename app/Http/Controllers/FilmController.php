<?php

namespace App\Http\Controllers;

use App\Http\Resources\ActorResource;
use Illuminate\Http\Request;
use App\Http\Resources\FilmResource;
use App\Http\Resources\FilmCriticsRessource;
use App\Models\Film;
use App\Models\User;
use App\Http\Requests\FilmRequest;
use Illuminate\Support\Facades\Auth;
use Exception;

class FilmController extends Controller
{
    public function index(Request $request)
    {
        $query = $request->query();
        $films = FilmResource::collection(Film::paginate(20));

        if($query != null)
        {
            $keywords = null;
            $rating = null;
            $maxLength = null;

            if(!empty($query['keywords']))
            {
                $keywords = $query['keywords'];
            }
            if(!empty($query['rating']))
            {
                $rating = $query['rating'];
            }
            if(!empty($query['max-length']))
            {
                $maxLength = $query['max-length'];
            }

            $films = $this->search($keywords, $rating, $maxLength, $films);
        }

        if($films->count() === 0)
        {
            abort(404, 'Aucun film trouvé selon le ou les critères choisis.');
        }
        else
        {
            return $films->response()->setStatusCode(200);
        }
    }

    public function show($id)
    {
        $film = Film::find($id);

        if($film != null)
        {
            $film = new FilmCriticsRessource($film);
            return $film->response()->setStatusCode(200);
        }

        abort(404);
    }

    public function showActors($id)
    {
        $film = Film::find($id);

        if($film != null)
        {
            $actors = $film->actors;

            return ActorResource::collection($actors)
                ->response()
                ->setStatusCode(200);
        }

        abort(404);
    }

    public function store(FilmRequest $request)
    {
        if(Auth::check())
        {
            try
            {
                $donnees = $request->validated();
                $userRoleId = auth()->user()->role_id;

                if($userRoleId == 1)
                {
                    $film = Film::create($donnees);
                    return (new FilmResource($film))->response()->setStatusCode(201);
                }
                else
                {
                    abort(403);
                }
            }
            catch(Exception $e)
            {
                abort(422,'Validation problem');
            }
        }
        else
        {
            abort(401);
        }
    }

    public function destroy($id)
    {
        if(Auth::check())
        {
            try
            {
                $userRoleId = auth()->user()->role_id;

                if($userRoleId === 1)
                {
                    $film = Film::Find($id);

                    if($film != null)
                    {
                        $film->delete()
                            ->response()
                            ->setStatusCode(201);
                    }
                }
                else
                {
                    abort(403);
                }
            }
            catch(Exception $e)
            {
                abort(404,'Film not found');
            }
        }
        else
        {
            abort(401);
        }
    }

    private function search($keywords, $rating, $maxLength, $films)
    {
        if($keywords != null)
        {
            $films = $this->searchTitleDescription($keywords);
        }
        if($rating != null)
        {
            $films = $this->searchRating($rating, $films);
        }
        if($maxLength != null)
        {
            $films = $this->searchMaxLength($maxLength, $films);
        }

        return $films;
    }

    private function searchTitleDescription($keywords)
    {
        return FilmResource::collection(Film::where(strtoupper('title'), 'like', strtoupper("%{$keywords}%"))
                    ->orWhere(strtolower('description'), 'like', strtolower("%{$keywords}%"))->paginate(20));
    }

    private function searchRating($rating, $films)
    {
        $rating = strtoupper($rating);
        $collection = $films->getCollection();
        $collection = $collection->where('rating', $rating);
        $films = $films->setCollection($collection);
        $films = FilmResource::collection($films);
        return $films;
    }

    private function searchMaxLength($maxLength, $films)
    {
        $collection = $films->getCollection();
        $collection = $collection->where('length', '<=', $maxLength);
        $films = $films->setCollection($collection);
        $films = FilmResource::collection($films);
        return $films;
    }
}
