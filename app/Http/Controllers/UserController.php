<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use Exception;

class UserController extends Controller
{
    public function register(UserRequest $request)
    {
        $donnees = $request->validated();
        $role = 2;

        if(!empty($donnees['role_id']))
        {
            $role = $donnees['role_id'];
        }

        try {
            $user = User::create([
                'login' => $donnees['login'],
                'password' => bcrypt($donnees['password']),
                'email' => $donnees['email'],
                'last_name' => $donnees['last_name'],
                'first_name' => $donnees['first_name'],
                'role_id' => $role
            ]);
        }
        catch(Exception $e){
            abort(422,'Validation problem');
        }

        return response()->json([
			'status' => 'Success',
			'message' => 'User created',
			'data' => [
                'token' => $user->createToken('API Token')->plainTextToken
            ]
		], 201);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->toArray())) {
            return $this->error('Wrong login or password', 401);
        }

        return response()->json([
			'status' => 'Success',
			'message' => 'Logged in',
			'data' => [
                'token' => Auth::user()->createToken('API Token')->plainTextToken
            ]
		], 200);
    }

    public function logout()
    {
        if(Auth::check())
        {
            auth()->user()->tokens()->delete();
            return response()->json([
                'status' => 'Success',
                'message' => 'Logged out'
                ], 200);
        }
        else
        {
            abort(400);
        }
    }

    public function show() {
        if(Auth::check())
        {
            return response()->json(auth()->user(), 200);
        }
        else
        {
            abort(401);
        }
    }
    public function update(UserRequest $request, $id)
    {
        if (Auth::check() && auth()->user()->id === $id)
        {
            $user = User::find($id);

            try {
                $donnees = $request->validated();
                $user['login'] = $donnees['login'];
                $user['email'] = $donnees['email'];
                $user['last_name'] = $donnees['last_name'];
                $user['first_name'] = $donnees['first_name'];
                $user->save();
                $this->updatePassword($request, $id);
            }
            catch(Exception $e)
            {
                abort(422,'Validation problem');
            }
            return response()->setStatusCode(201);
        }
        else
        {
            abort('Wrong login or password', 401);
        }
    }
    public function updatePassword(UserRequest $request, $id)
    {
        if (Auth::check() && auth()->user()->id === $id)
        {
            $donnees = $request->validated();
            $user = User::find($id);

            try {
                $user['password'] = bcrypt($donnees['password']);
                $user->save();
            }
            catch(Exception $e)
            {
                abort(422,'Validation problem');
            }
        }
    }
}
