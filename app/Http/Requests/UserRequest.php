<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|min:3',
            'password' => 'required|min:8',
            'email' => 'required|email',
            'last_name' => 'min:3',
            'first_name' => 'min:3',
            'role_id' => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Le login est requis.',
            'login.min' => 'Le login doit comporter au minimum 3 caractères.',
            'password.required' => 'Le mot de passe est requis',
            'password.min' => 'Le mot de passe doit comporter au minimum 8 caractères.',
            'email.required' => 'L\'adresse email est requise.',
            'email.date_format' => 'Le format de l\'adresse email doit être valide.',
            'last_name.required' => 'Le nom doit comporter au minimum 3 caractères.',
            'first_name.numeric' => 'Le prénom doit comporter au minimum 3 caractères.'
        ];
    }
}
