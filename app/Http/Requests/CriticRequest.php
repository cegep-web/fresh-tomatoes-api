<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CriticRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|numeric',
            'film_id' => 'required|numeric',
            'score' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'L\'utilisateur est requis.',
            'user_id.numeric' => 'L\'utilisateur doit être une valeur numérique.',
            'film_id.required' => 'Le film est requis.',
            'film_id.numeric' => 'Le film doit être une valeur numérique.',
            'score.required' => 'La score est requise.',
            'score.numeric' => 'Le score doit être une valeur décimale.'
        ];
    }
}
