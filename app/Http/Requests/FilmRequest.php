<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'release_year' => 'required|date_format:YYYY',
            'length' => 'required|numeric',
            'language_id' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Le titre est requis.',
            'title.min' => 'Le titre doit comporter au minimum 3 caractères.',
            'release_year.required' => 'L\'année de parution est requise.',
            'release_year.date_format' => 'Le format doit être de quatre chiffres repésentant une année.',
            'length.required' => 'La longueur du film est requise.',
            'length.numeric' => 'La longueur du film doit être une valeur numérique.',
            'language_id.required' => 'Le langage est requis.',
            'language_id.numeric' => 'Le langage du film doit être une valeur numérique.'
        ];
    }
}
