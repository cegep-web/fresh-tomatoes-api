<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('films','App\Http\Controllers\FilmController@index');
Route::get('films/{id}', '\App\Http\Controllers\FilmController@show');
Route::get('films/{id}/actors', '\App\Http\Controllers\FilmController@showActors');
Route::post('/users/register', 'App\Http\Controllers\UserController@register');
Route::post('/users/login', 'App\Http\Controllers\UserController@login');
Route::post('/users/logout', 'App\Http\Controllers\UserController@logout');

Route::group(['middleware' => 'auth:sanctum'], function () {
    //Films
    Route::post('films','App\Http\Controllers\FilmController@store');
    Route::delete('films/{id}', '\App\Http\Controllers\FilmController@destroy');

    //Users
    Route::get('/user', 'App\Http\Controllers\UserController@show');
    Route::put('/user', 'App\Http\Controllers\UserController@update');
    Route::put('/user/password', 'App\Http\Controllers\UserController@updatePassword');

    //Critics
    Route::post('critics','App\Http\Controller\CriticController@store');
});
